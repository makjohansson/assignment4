/** 
 * app.js
 * 
 * Handles all logic for the Komputer Store and featching data from the database 
 *       "https://noroff-komputer-store-api.herokuapp.com/computers"
 * 
*/

const bankBalanceElement = document.getElementById("balance");
const outstandingLoanElement = document.getElementById("outstandingLoan");
const outstandingLoanTextElement = document.getElementById("outstandingLoanText");
const outstandingLoanCurrencyElement = document.getElementById("outstandingLoanCurrency");
const loanBtnElement = document.getElementById("loanBtn");
const payElement = document.getElementById("pay");
const bankBtnElement = document.getElementById("bankBtn");
const workBtnElement = document.getElementById("workBtn");
const repayBtnElement = document.getElementById("repayBtn");
const laptopsElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features");
const imgElement = document.getElementById("laptopImg");
const laptopDescElement = document.getElementById("laptopDesc");
const laptopPriceElement = document.getElementById("laptopPrice");
const laptoptitleElement = document.getElementById("laptopTitle");
const laptopBuyNoWBtn = document.getElementById("laptopBuyBtn");

const salary = 100;
let laptops = [];
let bankBalance = 0.0;
let payBalance = 0.0;
let outstandingLoan = 0.0;
let applyedForOneLoanBeforeByingOneComputer = false;
let payedBack = true;
let laptopPrice = 0.0;
let laptopTitle = "";

async function getLaptops() {
    try{
        const response = await fetch("https://noroff-komputer-store-api.herokuapp.com/computers");
        laptops = await response.json();
        laptops.forEach(laptop => addToUl(laptop));
        addToFeatures(laptops[0]);
    } catch (error){
        console.log(error);
    }    
}

function addToUl(laptop) {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title))
    laptopsElement.appendChild(laptopElement);
}

function addToFeatures(laptop) {
    featuresElement.innerHTML = "";
    laptop.specs.forEach(spec => {
        const specLi = document.createElement("li");
        specLi.innerText = spec;
        featuresElement.appendChild(specLi);
    });
    laptopPrice = parseInt(laptop.price);
    laptopTitle = laptop.title;
    laptoptitleElement.innerText = laptopTitle;
    imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptop.image;
    laptopDescElement.innerText = laptop.description;
    laptopPriceElement.innerText = laptopPrice + " kr"
}

/**
 * Handles the logic for a users input when getting a loan
 * @returns noting if input from user is not a number
 */
function getLoan() {
    const amount = parseInt(prompt("Enter amount for loan: "));
    if (isNaN(amount)){
        return
    }
    let maxAmount = bankBalance > 0 ? (bankBalance / 2) : 0.0;
    if (amount > maxAmount) {
        alert(`Max amount you can apply for is ${maxAmount}, work some more,  put the cash in the bank and try again`);
    } else {
        outstandingLoan = amount;
        bankBalance += parseInt(outstandingLoan);
        payedBack = false;
        applyedForOneLoanBeforeByingOneComputer = true;
        outstandingLoanElement.innerText = outstandingLoan;
        bankBalanceElement.innerText = bankBalance;
        outstandingLoanVisibility(true);
        repayBtnVisibility(true)
    }
}


function payLoan() {
    let interest = payBalance * 0.1;
    if (outstandingLoan > interest) {
        outstandingLoan -= interest;
        bankBalance += payBalance * 0.9;
    } else {
        bankBalance += payBalance - outstandingLoan;
        outstandingLoan = 0.0;
    }
}

/**
 * Hide or show the outstandingLoan elements
 * @param {boolean} show True if outstandingLoan elements should be visible
 */
function outstandingLoanVisibility(show) {
    outstandingLoanElement.style.visibility = show ? "visible" : "hidden";
    outstandingLoanTextElement.style.visibility = show ? "visible" : "hidden";
    outstandingLoanCurrencyElement.style.visibility = show ? "visible" : "hidden";
}

/**
 * Hide or show the repayBtn
 * @param {boolean} show True if the repayBtn should be visible
 */
function repayBtnVisibility(show) {
    repayBtnElement.style.visibility = show ? "visible" : "hidden";
}

function imgError(){
    imgElement.src = "https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg";
}

const handleLaptopChange = () => {
    const laptop = laptops[laptopsElement.selectedIndex];
    addToFeatures(laptop);
}

// Handles the salary for a worker, increas the pay with 100 kr per click
const handleWork = () => {
    payBalance += salary;
    payElement.innerText = payBalance;
}

// Handles get a loan btn clicked
const handleLoan = () => {
    if (applyedForOneLoanBeforeByingOneComputer) {
        alert("You cannot get more than one bank loan before buying a computer!");
    } else if (!payedBack) {
        alert("You must pay it back BEFORE getting another loan!")
    } else if(bankBalance <= 0) {
        alert("You do not have any money, go do some work")
    } else {
        getLoan();
    }
}

// Handles the repay loan logic
const handleRepayLoan = () => {
    if (outstandingLoan < payBalance) {
        payBalance -= outstandingLoan;
        outstandingLoan = 0.0;
        outstandingLoanVisibility(false);
        repayBtnVisibility(false);
        payedBack = true;
    } else {
        outstandingLoan -= payBalance;
        payBalance = 0.0;
        payedBack = true;
    }
    console.log(payBalance);
    payElement.innerText = payBalance;
    outstandingLoanElement.innerText = outstandingLoan;
}

// Handles the put into bank logic
const handlePutIntoBank = () => {
    if (payBalance == 0){
        return
    }
    if (outstandingLoan > 0){
        payLoan();
    } else {
        bankBalance += payBalance;
    }
    payBalance = 0.0;
    if (outstandingLoan <= 0) {
        repayBtnVisibility(false);
        payedBack = true;
    }
    
    bankBalanceElement.innerText = bankBalance;
    if (outstandingLoan > 0)  {
        outstandingLoanElement.innerText = outstandingLoan;
    }
    else{
        outstandingLoanVisibility(false);
        outstandingLoan = 0.0;
    }
    payElement.innerText = payBalance;
}

// Handles the buy a laptop logic
const handleBuyLaptop = () => {
    if (laptopPrice > bankBalance) {
        alert(`You need ${laptopPrice - bankBalance} kr more to buy this laptop, ` + 
         `go work some more or take a loan`)
    } else {
        bankBalance -= laptopPrice;
        bankBalanceElement.innerText = bankBalance;
        applyedForOneLoanBeforeByingOneComputer = false;
        alert(`Congratulation, you are now a proude owner of the ${laptopTitle}`)
    }
}


// Setup for first/reload entry on page
repayBtnElement.style.visibility = "hidden";
outstandingLoanVisibility(false);
bankBalanceElement.innerText = bankBalance;
payElement.innerText = payBalance;
getLaptops();

workBtnElement.addEventListener("click", handleWork);
loanBtnElement.addEventListener("click", handleLoan);
bankBtnElement.addEventListener("click", handlePutIntoBank);
repayBtnElement.addEventListener("click", handleRepayLoan);
laptopsElement.addEventListener("change", handleLaptopChange);
laptopBuyNoWBtn.addEventListener("click", handleBuyLaptop);